package org.katas;


public enum HeroElement {
    Water,
    Fire,
    Earth;

    private HeroElement advantageElement;
    private HeroElement disadvantageElement;
    public static final double ADVANTAGE_ATTACK_COEF = 1.2;
    public static final double DISADVANTAGE_ATTACK_COEF = 0.8;

    static {
        Water.disadvantageElement = Earth;
        Water.advantageElement = Fire;
        Fire.disadvantageElement = Water;
        Fire.advantageElement = Earth;
        Earth.disadvantageElement = Fire;
        Earth.advantageElement = Water;
    }

    public boolean isAdvantageElement(HeroElement element){
        return element.equals(advantageElement);
    }

    public boolean isDisadvantageElement(HeroElement element){
        return element.equals(disadvantageElement);
    }
}
