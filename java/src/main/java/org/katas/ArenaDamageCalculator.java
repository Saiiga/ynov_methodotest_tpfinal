package org.katas;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static java.lang.Math.*;
import static org.katas.HeroElement.ADVANTAGE_ATTACK_COEF;
import static org.katas.HeroElement.DISADVANTAGE_ATTACK_COEF;

public class ArenaDamageCalculator {

    public static final double LETHAL_COEF = 5000;
    public static final double DEFENSE_COEF = 7500;
    public static final double BASE_LETHAL_MULTIPLICATIVE = 0.5;

    public List<Hero> computeDamage(Hero attacker, List<Hero> defenders) {
        if (checkInvalidStatement(attacker, defenders))
            return Collections.emptyList();

        Hero targetedHero = getNextTarget(attacker, defenders);

        double damageInflicted = getDamageInflicted(attacker, targetedHero);

        handleDamage(targetedHero, damageInflicted);

        return defenders;
    }

    private boolean checkInvalidStatement(Hero attacker, List<Hero> defenders) {
        return attacker.getLp() == 0 || defenders == null || defenders.isEmpty();
    }

    private void handleDamage(Hero targetedHero, double damageInflicted) {
        // Ensure that life point is never negative
        damageInflicted = max(0, damageInflicted);

        damageInflicted = floor(damageInflicted);
        targetedHero.setLp(reduceLP(targetedHero, damageInflicted)); // Reduce the attacked hero's LP by the damage inflicted

    }

    private Hero getNextTarget(Hero attacker, List<Hero> defenders) {
        var disadvantagedHeroes = new ArrayList<Hero>();
        var neutralHeroes = new ArrayList<Hero>();
        var advantagedHeroes = new ArrayList<Hero>();

        //Will add in each list a hero, following his elementary advantage and disadvantage
        for (var hero : defenders) {
            if (hero.getLp() == 0)
                continue;
            if (attacker.isInAdvantage(hero))
                disadvantagedHeroes.add(hero);
            else if (attacker.isInDisadvantage(hero))
                advantagedHeroes.add(hero);
            else
                neutralHeroes.add(hero);
        }

        return chooseTarget(disadvantagedHeroes, neutralHeroes, advantagedHeroes);
    }

    private Hero chooseTarget(ArrayList<Hero> disadvantagedHeroes, ArrayList<Hero> neutralHeroes, ArrayList<Hero> advantagedHeroes) {
        if (!disadvantagedHeroes.isEmpty())
            return getRandomHeroInList(disadvantagedHeroes);
        else if (!neutralHeroes.isEmpty())
            return getRandomHeroInList(neutralHeroes);
        else
            return getRandomHeroInList(advantagedHeroes);
    }

    private Hero getRandomHeroInList(ArrayList<Hero> heroes) {
        return heroes.get((int) floor(random() * heroes.size()));
    }

    private double getDefensiveMultiplication(Hero targetedHero) {
        if (targetedHero.getBuffs().contains(Buff.Defense))
            return (1 - targetedHero.getDef() / DEFENSE_COEF) * Buff.Defense.coefficient;
        return 1 - targetedHero.getDef() / DEFENSE_COEF;
    }

    private double getAttackCoefficient(Hero attacker, Hero targetedHero) {
        double damage;
        damage = attacker.getPow();
        if (attacker.getBuffs().contains(Buff.Attack))
            damage *= Buff.Attack.coefficient;

        if (attacker.getBuffs().contains(Buff.Holy))
            return damage * Buff.Holy.coefficient;


        return advantageCalculation(damage, attacker, targetedHero);
    }

    private long getCriticalDamages(Hero attacker, Hero targetedHero) {
        return round(getAttackCoefficient(attacker, targetedHero) * (1 + BASE_LETHAL_MULTIPLICATIVE + attacker.getLeth() / LETHAL_COEF));
    }

    private double getDamageInflicted(Hero attacker, Hero targetedHero) {
        double damage = getDamages(attacker, targetedHero);
        if (attacker.getBuffs().contains(Buff.Holy))
            return damage;

        return damage * getDefensiveMultiplication(targetedHero);
    }

    private double getDamages(Hero attacker, Hero targetedHero) {
        if (isCriticalAttack(attacker)) {
            return getCriticalDamages(attacker, targetedHero);
        } else {
            return getAttackCoefficient(attacker, targetedHero);
        }
    }

    private boolean isCriticalAttack(Hero attacker) {
        return random() * 100 < attacker.getCrtr();
    }

    private double advantageCalculation(double damage, Hero attacker, Hero targetedHero) {
        if (attacker.isInAdvantage(targetedHero))
            return damage * ADVANTAGE_ATTACK_COEF;
        if (attacker.isInDisadvantage(targetedHero))
            return damage * DISADVANTAGE_ATTACK_COEF;
        return damage;
    }

    private int reduceLP(Hero targetedHero, double damageInflicted) {
        return max(targetedHero.getLp() - (int) damageInflicted, 0);
    }
}
