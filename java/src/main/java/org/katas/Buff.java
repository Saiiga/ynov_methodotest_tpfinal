package org.katas;

public enum Buff {
    Attack(1.25),
    Defense(0.75),
    Holy(.8),
    TurnCoat();

    public final double coefficient;

    Buff(double coefficient)
    {
        this.coefficient = coefficient;
    }
    Buff(){coefficient = 0;}
}
