package org.katas;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class BuffTest {

    ArenaDamageCalculator arenaDamageCalculator = new ArenaDamageCalculator();

    @Test
    public void BuffAttack_bonusIsApplied() {
        //Given
        Hero attackHero = new Hero(HeroElement.Fire,20,0,0,0,100);
        attackHero.setBuffs(List.of(Buff.Attack));

        Hero defenseHero = new Hero(HeroElement.Fire, 10, 0, 0, 0, 100);
        List<Hero> defenseHeroes = List.of(defenseHero);

        //When
        arenaDamageCalculator.computeDamage(attackHero,defenseHeroes);

        //Then
        assertEquals(75, defenseHero.getLp());
    }

    @Test
    public void BuffDefense_bonusIsApplied() {
        //Given
        Hero attackHero = new Hero(HeroElement.Fire,20,0,0,0,100);

        Hero defenseHero = new Hero(HeroElement.Fire, 10, 0, 0, 0, 100);
        defenseHero.setBuffs(List.of(Buff.Defense));
        List<Hero> defenseHeroes = List.of(defenseHero);

        //When
        arenaDamageCalculator.computeDamage(attackHero,defenseHeroes);

        //Then
        assertEquals(85, defenseHero.getLp());
    }

    @Test
    public void BuffAttackAndDefense_2bonusIsApplied() {
        //Given
        Hero attackHero = new Hero(HeroElement.Fire,20,0,0,0,100);
        attackHero.setBuffs(List.of(Buff.Attack));

        Hero defenseHero = new Hero(HeroElement.Fire, 10, 0, 0, 0, 100);
        defenseHero.setBuffs(List.of(Buff.Defense));
        List<Hero> defenseHeroes = List.of(defenseHero);

        //When
        arenaDamageCalculator.computeDamage(attackHero,defenseHeroes);

        //Then
        assertEquals(82, defenseHero.getLp());
    }

    @Test
    public void MultipleBuffAttack_1bonusIsApplied() {
        //Given
        Hero attackHero = new Hero(HeroElement.Fire,20,0,0,0,100);
        attackHero.setBuffs(List.of(Buff.Attack,Buff.Attack,Buff.Attack,Buff.Attack));

        Hero defenseHero = new Hero(HeroElement.Fire, 10, 0, 0, 0, 100);
        List<Hero> defenseHeroes = List.of(defenseHero);

        //When
        arenaDamageCalculator.computeDamage(attackHero,defenseHeroes);

        //Then
        assertEquals(75, defenseHero.getLp());
    }

    @Test
    public void MultipleBuffDefense_1bonusIsApplied() {
        //Given
        Hero attackHero = new Hero(HeroElement.Fire,20,0,0,0,100);

        Hero defenseHero = new Hero(HeroElement.Fire, 10, 0, 0, 0, 100);
        defenseHero.setBuffs(List.of(Buff.Defense,Buff.Defense,Buff.Defense,Buff.Defense));
        List<Hero> defenseHeroes = List.of(defenseHero);

        //When
        arenaDamageCalculator.computeDamage(attackHero,defenseHeroes);

        //Then
        assertEquals(85, defenseHero.getLp());
    }

    @Test
    public void BuffAttackAndCritical_bonusIsAppliedToCriticalDamage()
    {
        //Given
        Hero attackHero = new Hero(HeroElement.Fire, 20, 0, 0, 100, 10);
        Hero defenseHero = new Hero(HeroElement.Fire, 0, 0, 0, 0, 100);
        attackHero.setBuffs(List.of(Buff.Attack));

        //When
        arenaDamageCalculator.computeDamage(attackHero, List.of(defenseHero));

        //Then
        assertEquals(62,defenseHero.getLp());
    }

    @Test
    public void BuffAttackAndCritical_bonusIsAppliedToCriticalDamageWithDef()
    {
        //Given
        Hero attackHero = new Hero(HeroElement.Fire, 20, 0, 0, 100, 10);
        Hero defenseHero = new Hero(HeroElement.Fire, 0, 3750, 0, 0, 100);
        attackHero.setBuffs(List.of(Buff.Attack));

        //When
        arenaDamageCalculator.computeDamage(attackHero, List.of(defenseHero));

        //Then
        assertEquals(81,defenseHero.getLp());
    }

    @Test
    void BuffHoly_bonusIsApplied_With0Defense_withAdvantage()
    {
        //Given
        Hero attackHero = new Hero(HeroElement.Fire,20,0,0,0,100);
        attackHero.setBuffs(List.of(Buff.Holy));

        Hero defenseHero = new Hero(HeroElement.Earth, 10, 0, 0, 0, 100);
        List<Hero> defenseHeroes = List.of(defenseHero);

        //When
        arenaDamageCalculator.computeDamage(attackHero,defenseHeroes);

        //Then
        assertEquals(84, defenseHero.getLp());
    }

    @Test
    void BuffHoly_bonusIsApplied_WithDefenseAbleToHalveDamage_withAdvantage()
    {
        //Given
        Hero attackHero = new Hero(HeroElement.Fire,20,0,0,0,100);
        attackHero.setBuffs(List.of(Buff.Holy));

        Hero defenseHero = new Hero(HeroElement.Earth, 10, 3750, 0, 0, 100);
        List<Hero> defenseHeroes = List.of(defenseHero);

        //When
        arenaDamageCalculator.computeDamage(attackHero,defenseHeroes);

        //Then
        assertEquals(84, defenseHero.getLp());
    }

    @Test
    void BuffHoly_bonusIsApplied_With0Defense_withDisadvantage()
    {
        //Given
        Hero attackHero = new Hero(HeroElement.Earth,20,0,0,0,100);
        attackHero.setBuffs(List.of(Buff.Holy));

        Hero defenseHero = new Hero(HeroElement.Fire, 10, 0, 0, 0, 100);
        List<Hero> defenseHeroes = List.of(defenseHero);

        //When
        arenaDamageCalculator.computeDamage(attackHero,defenseHeroes);

        //Then
        assertEquals(84, defenseHero.getLp());
    }

    @Test
    void BuffHoly_bonusIsApplied_WithDefenseAbleToHalveDamage_withDisadvantage()
    {
        //Given
        Hero attackHero = new Hero(HeroElement.Earth,20,0,0,0,100);
        attackHero.setBuffs(List.of(Buff.Holy));

        Hero defenseHero = new Hero(HeroElement.Fire, 10, 3750, 0, 0, 100);
        List<Hero> defenseHeroes = List.of(defenseHero);

        //When
        arenaDamageCalculator.computeDamage(attackHero,defenseHeroes);

        //Then
        assertEquals(84, defenseHero.getLp());
    }


    private static Stream<Arguments> dataProviderForBuffTurnCoat()
    {
        return Stream.of(
                Arguments.of(HeroElement.Fire,HeroElement.Water),
                Arguments.of(HeroElement.Water,HeroElement.Earth),
                Arguments.of(HeroElement.Earth,HeroElement.Fire)
        );
    }

    @ParameterizedTest
    @MethodSource("dataProviderForBuffTurnCoat")
        void BuffTurnCoat_bonusIsApplied_shouldInverseDisadvantageAndAdvantage(HeroElement attackerElement, HeroElement defenseElement)
    {
        Hero attackHero = new Hero(attackerElement,20,0,0,0,100);
        attackHero.setBuffs(List.of(Buff.TurnCoat));

        Hero defenseHero = new Hero(defenseElement, 10, 0, 0, 0, 100);
        List<Hero> defenseHeroes = List.of(defenseHero);

        //When
        arenaDamageCalculator.computeDamage(attackHero,defenseHeroes);

        //Then
        assertEquals(76, defenseHero.getLp(),attackerElement.name() +" now powerfull again " + defenseElement.name());
    }

    @ParameterizedTest
    @MethodSource("dataProviderForBuffTurnCoat")
    void BuffTurnCoat_bonusIsApplied_shouldInverseAdvantageAndDisadvantage(HeroElement defenseElement, HeroElement attackerElement)
    {
        Hero attackHero = new Hero(attackerElement,20,0,0,0,100);
        attackHero.setBuffs(List.of(Buff.TurnCoat));

        Hero defenseHero = new Hero(defenseElement, 10, 0, 0, 0, 100);
        List<Hero> defenseHeroes = List.of(defenseHero);

        //When
        arenaDamageCalculator.computeDamage(attackHero,defenseHeroes);

        //Then
        assertEquals(84, defenseHero.getLp(),attackerElement.name() +" now weak again " + defenseElement.name());

    }
}
