package org.katas.utils;

import org.katas.Hero;
import org.katas.HeroElement;

import java.util.ArrayList;
import java.util.List;

public class HeroBuilder {

    public static List<Hero> generateDefenders(int lp, HeroElement... elements) {
        List<Hero> heros = new ArrayList<>();

        for (HeroElement element : elements)
            heros.add(new Hero(element, 10, 0, 10, 0, lp));

        return heros;
    }
}
