package org.katas;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class HeroTest
{
    
    @Test
    void testGetterAndSetter()
    {
        var defaultStats = 0;
        var newStats = 100;
        var newElement = HeroElement.Water;
        var buff = Buff.Attack;
        var hero = new Hero(HeroElement.Earth,defaultStats,defaultStats,defaultStats,defaultStats,defaultStats);
        
        hero.setCrtr(newStats);
        hero.setDef(newStats);
        hero.setLp(newStats);
        hero.setLeth(newStats);
        hero.setPow(newStats);
        hero.setElement(newElement);
        hero.setBuffs(List.of(buff));
        
        assertEquals(newStats,hero.getCrtr(), "Crtr should be equals to "+newStats);
        assertEquals(newStats,hero.getDef(), "Def should be equals to "+newStats);
        assertEquals(newStats,hero.getLp(), "LP should be equals to "+newStats);
        assertEquals(newStats,hero.getLeth(), "Leth should be equals to "+newStats);
        assertEquals(newStats,hero.getPow(), "Pow should be equals to "+newStats);
        assertEquals(newElement,hero.getElement(), "New Element should be equals to "+newElement.name());
        assertTrue(hero.getBuffs().contains(buff), "Buff list should contains "+ buff.name());
    }
}
