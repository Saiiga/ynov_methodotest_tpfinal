package org.katas;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.katas.utils.HeroBuilder;

import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.katas.HeroElement.ADVANTAGE_ATTACK_COEF;
import static org.katas.HeroElement.DISADVANTAGE_ATTACK_COEF;

class ArenaDamageCalculatorTest {
    private final ArenaDamageCalculator area = new ArenaDamageCalculator();

    private static final int ATTACKING_HERO_POWER = 50;
    private static final int ATTACKING_HERO_LP = 100;
    private static final int DEFENSIVE_HERO_POWER = 0;
    private static final int DEFENSIVE_HERO_LP = 100;

    private static final int DISADVANTAGED_ELEMENT_INDEX = 0;
    private static final int SAME_ELEMENT_INDEX = 1;
    private static final int ADVANTAGED_ELEMENT_INDEX = 2;


    /**
     * Attacking hero power is set to 50 and defensive hero defense is set to 0
     * defensive hero lp should be 50 less than before
     */
    @Test
    void attackingHero_should_doFiftyDamage_to_defensiveHero_with_zeroDefense() {
        var attackingHero = new Hero(HeroElement.Fire, ATTACKING_HERO_POWER, 0, 0, 0, ATTACKING_HERO_LP);
        var defensiveHero = new Hero(HeroElement.Fire, DEFENSIVE_HERO_POWER, 0, 0, 0, DEFENSIVE_HERO_LP);

        area.computeDamage(attackingHero, List.of(defensiveHero));

        assertEquals( DEFENSIVE_HERO_LP - ATTACKING_HERO_POWER,defensiveHero.getLp());
    }

    /**
     * Attacking hero should kill defensive hero because it has the same power
     * but Defensive hero has 3750 defense which equates to receiving half of the damage
     */
    @Test
    void defensiveHero_should_receiveHalfDamage() {
        var attackingHero = new Hero(HeroElement.Fire, ATTACKING_HERO_POWER, 0, 0, 0, ATTACKING_HERO_LP);
        var defensiveHero = new Hero(HeroElement.Fire, DEFENSIVE_HERO_POWER, 3750, 0, 0, DEFENSIVE_HERO_LP);

        area.computeDamage(attackingHero, List.of(defensiveHero));

        assertEquals(DEFENSIVE_HERO_LP - (ATTACKING_HERO_POWER / 2),defensiveHero.getLp());
    }

    /**
     * Attacking hero should always do a critical hit because he has 100% chance of doing so
     * and has a lethality of 2500 which equates to double the damage.
     * So Attacking hero should always do double the damage
     */
    @Test
    void defensiveHero_should_receiveDoubleDamage() {
        var attackingHero = new Hero(HeroElement.Fire, ATTACKING_HERO_POWER, 0, 2500, 100, ATTACKING_HERO_LP);
        var defensiveHero = new Hero(HeroElement.Fire, DEFENSIVE_HERO_POWER, 0, 0, 0, DEFENSIVE_HERO_LP);

        area.computeDamage(attackingHero, List.of(defensiveHero));

        assertEquals(0,defensiveHero.getLp());
    }

    /**
     * Attacking hero has the same attacking power as defensive hero lp and defensive hero has 0 defense
     * So defensive hero should have 0 Lp
     */
    @Test
    void defensiveHero_should_haveZeroLP() {
        var attackingHero = new Hero(HeroElement.Fire, ATTACKING_HERO_POWER * 2, 0, 0, 0, ATTACKING_HERO_LP);
        var defensiveHero = new Hero(HeroElement.Fire, DEFENSIVE_HERO_POWER, 0, 0, 0, DEFENSIVE_HERO_LP);

        area.computeDamage(attackingHero, List.of(defensiveHero));

        assertEquals(0,defensiveHero.getLp());
    }

    /**
     * Attacking hero does more damage than defensive hero Lp but Lp can't be negative, so it should be
     * zero
     */
    @Test
    void defensiveHeroLp_should_notBeNegative() {

        var attackingHero = new Hero(HeroElement.Fire, ATTACKING_HERO_POWER, 0, 0, 0, ATTACKING_HERO_LP);
        var defensiveHero = new Hero(HeroElement.Fire, DEFENSIVE_HERO_POWER, 0, 0, 0, DEFENSIVE_HERO_LP / 4);

        area.computeDamage(attackingHero, List.of(defensiveHero));

        assertEquals(0,defensiveHero.getLp());
    }

    /*
     * Targeting Hero tests
     */

    /**
     * Data will be in this order :
     * Hero's element to test
     * list of Hero to target, in this order "advantage element, same element, disadvantage element"
     */
    private static Stream<Arguments> dataProviderForTargetingTest() {

        return Stream.of(
                Arguments.of(HeroElement.Water, HeroBuilder.generateDefenders(DEFENSIVE_HERO_LP, HeroElement.Fire, HeroElement.Water, HeroElement.Earth)),
                Arguments.of(HeroElement.Fire, HeroBuilder.generateDefenders(DEFENSIVE_HERO_LP, HeroElement.Earth, HeroElement.Fire, HeroElement.Water)),
                Arguments.of(HeroElement.Earth, HeroBuilder.generateDefenders(DEFENSIVE_HERO_LP, HeroElement.Water, HeroElement.Earth, HeroElement.Fire))
        );
    }

    @ParameterizedTest
    @MethodSource("dataProviderForTargetingTest")
    void attacker_shouldTarget_anEnemyWith_elementaryWeaknessAdvantage(HeroElement attackerElement, List<Hero> defenderHeros) {
        var attacker = new Hero(attackerElement, ATTACKING_HERO_POWER, 10, 10, 0, 10);

        area.computeDamage(attacker, defenderHeros);

        assertEquals((DEFENSIVE_HERO_LP - (ATTACKING_HERO_POWER * ADVANTAGE_ATTACK_COEF)), defenderHeros.get(DISADVANTAGED_ELEMENT_INDEX).getLp(), "Disadvantaged hero should be targeted first again by " + attacker);
        assertEquals(DEFENSIVE_HERO_LP, defenderHeros.get(SAME_ELEMENT_INDEX).getLp(), "Same element hero shouldn't being target");
        assertEquals(DEFENSIVE_HERO_LP, defenderHeros.get(ADVANTAGED_ELEMENT_INDEX).getLp(), "Advantaged hero shouldn't being target");
    }

    @ParameterizedTest
    @MethodSource("dataProviderForTargetingTest")
    void attacker_shouldTarget_anEnemyWithSame_elementary(HeroElement attackerElement, List<Hero> defenderHeros) {
        var attacker = new Hero(attackerElement, ATTACKING_HERO_POWER, 10, 10, 0, 10);

        area.computeDamage(attacker, List.of(defenderHeros.get(SAME_ELEMENT_INDEX), defenderHeros.get(ADVANTAGED_ELEMENT_INDEX)));

        assertEquals(DEFENSIVE_HERO_LP - ATTACKING_HERO_POWER, defenderHeros.get(SAME_ELEMENT_INDEX).getLp(), "Same element hero should be targeted first again by " + attacker);
        assertEquals(DEFENSIVE_HERO_LP, defenderHeros.get(ADVANTAGED_ELEMENT_INDEX).getLp(), "Advantaged element hero shouldn't be targeted");
    }

    @ParameterizedTest
    @MethodSource("dataProviderForTargetingTest")
    void attacker_shouldTarget_anEnemyWith_elementaryDisadvantage(HeroElement attackerElement, List<Hero> defenderHeros) {
        var attacker = new Hero(attackerElement, ATTACKING_HERO_POWER, 10, 10, 0, 10);

        area.computeDamage(attacker, List.of(defenderHeros.get(ADVANTAGED_ELEMENT_INDEX)));

        assertEquals((DEFENSIVE_HERO_LP - (ATTACKING_HERO_POWER * DISADVANTAGE_ATTACK_COEF)), defenderHeros.get(ADVANTAGED_ELEMENT_INDEX).getLp(), "Disadvantaged hero should be targeted first again by " + attacker);
    }


    @ParameterizedTest
    @MethodSource("dataProviderForTargetingTest")
    void attacker_shouldTarget_anEnemyWith_sameElement_ifHeroWithAdvantageIfDead(HeroElement attackerElement, List<Hero> defenderHeros) {
        var attacker = new Hero(attackerElement, ATTACKING_HERO_POWER, 10, 10, 0, 10);
        defenderHeros.get(DISADVANTAGED_ELEMENT_INDEX).setLp(0);

        area.computeDamage(attacker, defenderHeros);

        assertEquals(DEFENSIVE_HERO_LP - ATTACKING_HERO_POWER, defenderHeros.get(SAME_ELEMENT_INDEX).getLp(), "In case of disadvantaged element hero dead, same element hero should being targeted");
        assertEquals(DEFENSIVE_HERO_LP, defenderHeros.get(ADVANTAGED_ELEMENT_INDEX).getLp(), "Advantaged element hero shouldn't being targeted");
    }

    @ParameterizedTest
    @MethodSource("dataProviderForTargetingTest")
    void attacker_shouldTarget_anEnemyWith_elementDisadvantage_ifNoOtherHeroAlive(HeroElement attackerElement, List<Hero> defenderHeros) {
        var attacker = new Hero(attackerElement, ATTACKING_HERO_POWER, 10, 10, 0, 10);
        defenderHeros.get(DISADVANTAGED_ELEMENT_INDEX).setLp(0);
        defenderHeros.get(SAME_ELEMENT_INDEX).setLp(0);

        area.computeDamage(attacker, defenderHeros);

        assertEquals(DEFENSIVE_HERO_LP - (ATTACKING_HERO_POWER * DISADVANTAGE_ATTACK_COEF),defenderHeros.get(ADVANTAGED_ELEMENT_INDEX).getLp());

    }

    @Test
    void attacker_shouldNotAttack_whenLP_equalsToZero() {
        var attackingHero = new Hero(HeroElement.Fire, ATTACKING_HERO_POWER, 0, 0, 0, 0);
        var defensiveHero = new Hero(HeroElement.Fire, DEFENSIVE_HERO_POWER, 0, 0, 0, DEFENSIVE_HERO_LP);

        area.computeDamage(attackingHero, List.of(defensiveHero));

        assertEquals(DEFENSIVE_HERO_LP, defensiveHero.getLp());
    }

}